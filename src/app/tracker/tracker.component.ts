import {AfterViewInit, Component, OnInit} from '@angular/core';
import {ServiceExternalService} from "../service-external.service";
import {ActivatedRoute, Params, Router} from "@angular/router";
import {data} from "autoprefixer";

@Component({
  selector: 'app-tracker',
  templateUrl: './tracker.component.html',
  styleUrls: ['./tracker.component.css']
})
export class TrackerComponent implements OnInit, AfterViewInit{

 public package_data : any = null;
  package_id : string;
  constructor(private service : ServiceExternalService, private router : ActivatedRoute) {}

   ngOnInit() {
    this.router.params.subscribe((data : any)=>{
      this.package_id = data.package_id
    })
     this.service.findPackage(this.package_id).subscribe((response : any)=>{
       this.package_data = response.data
       if (response?.data?.active_delivery_id){
         this.service.getNewEventDeliveryUpdated(response.data.active_delivery_id);
         this.service.findDelivery(response.data.active_delivery_id).subscribe((res : any)=>{
           this.package_data.delivery = res.data;
           this.package_data.location = res.data.location;
           this.service.updateData(this.package_data);
         })
       }else{
         this.service.updateData(this.package_data);
         this.service.getNewEventDeliveryUpdated("0")
       }

     })


  }

  ngAfterViewInit() {
    this.service.event$.subscribe((position:any)=>{
      if(position && this.package_data){
        this.package_data.location = position
        this.service.updateData(this.package_data);
      }
    })
  }

}
