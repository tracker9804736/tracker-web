import {Component, OnInit, ViewChild, ElementRef, AfterViewInit, OnDestroy, Input} from '@angular/core';
import { Map, NavigationControl,Marker, Popup } from 'maplibre-gl';
import {ServiceExternalService} from "../service-external.service";
import {Subscription} from "rxjs";

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements OnInit, AfterViewInit, OnDestroy{
  @Input() data : any;

  private subscription: Subscription;
  map: Map | undefined;
  @ViewChild('map')
  private mapContainer!: ElementRef<HTMLElement>;

  constructor(private service : ServiceExternalService) { }
  ngOnInit() {

  }

  async ngAfterViewInit() {
    const initialState = { lng: 2.35246, lat:6.4838, zoom: 10 };
    this.map = new Map({
      container: this.mapContainer.nativeElement,
      style: `https://api.maptiler.com/maps/streets-v2/style.json?key=7PN8GeBgUUTE95HwO0rV`,
      center: [initialState.lng, initialState.lat],
      zoom: initialState.zoom
    });

    var myMarkers :any[] = [];

    this.subscription =  await this.service.data$.subscribe((data:any)=>{

      // remove markers yet
      if (myMarkers.length > 0) {
        for (var i = myMarkers.length - 1; i >= 0; i--) {
          myMarkers[i].remove();
        }
      }

      if (data && this.map){

        let to_location = null;
        let from_location = null;
        let title_from_location = data?.from_address;
        let title_to_location = data?.to_address;
        if (data.package){
          to_location =  data.package.to_location;
          from_location = data.package.from_location;
          title_from_location = data.package.from_address;
          title_to_location = data.package.to_address;
        }


        //package infos
        if (data.hasOwnProperty('from_location') || from_location){
          const m3 =  new Marker({color: "#FF0000"})
            .setPopup(new Popup().setHTML(`<h1>${title_from_location}</h1>`))
            .setLngLat([parseFloat(data?.from_location?.lng || from_location?.lng), parseFloat(data?.from_location?.lat || from_location?.lat)])
            .addTo(this.map);
          myMarkers.push(m3);
        }

        //package infos
        if (data.hasOwnProperty('to_location') || to_location){
         const m1 = new Marker({color: "#1b8c03"})
            .setPopup(new Popup().setHTML(`<h1>${title_to_location}</h1>`))
            .setLngLat([parseFloat(data?.to_location?.lng || to_location?.lng), parseFloat(data?.to_location?.lat || to_location?.lat)])
            .addTo(this.map);
          myMarkers.push(m1);
        }


        //delivery info
        if (data.hasOwnProperty('location')){
          const m2 = new Marker({color: "#eabc31"})
            .setLngLat([parseFloat(data?.location?.lng), parseFloat(data?.location?.lat)])
            .addTo(this.map);
          myMarkers.push(m2);
        }



      }



   })

    this.map?.addControl(new NavigationControl(), 'top-right');


  }

  ngOnDestroy() {
    this.map?.remove();
    if (this.subscription) {
      this.subscription
    }
  }

}
