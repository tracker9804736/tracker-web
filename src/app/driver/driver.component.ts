import {AfterViewInit, Component, Input, OnInit} from '@angular/core';
import {ServiceExternalService} from "../service-external.service";
import {ActivatedRoute} from "@angular/router";
import {Observable} from "rxjs";
import {ToastrService} from "ngx-toastr";

@Component({
  selector: 'app-driver',
  templateUrl: './driver.component.html',
  styleUrls: ['./driver.component.css']
})
export class DriverComponent implements OnInit, AfterViewInit {
   delivery_data : any;

  private delivery_id : string;
  private currentlocation: { lat: number; lng: number };
  isPickUp: boolean = false;
  isInTransit: boolean = false;
  isDelivery: boolean = false;
  isFailed: boolean = false;

  constructor(private service : ServiceExternalService, private router : ActivatedRoute, private toastr: ToastrService) {}

  ngAfterViewInit() {

  }

  ngOnInit(): void {
    this.router.params.subscribe((data : any)=>{
      this.delivery_id = data.delivery_id
    })

    this.service.findDelivery(this.delivery_id).subscribe((res : any)=>{
      this.delivery_data = res.data;

      this.isPickUp = (this.delivery_data?.status == "Open")
      this.isInTransit = (this.delivery_data?.status == "Picked_Up")
      this.isDelivery = (this.delivery_data?.status == "In_transit")
      this.isFailed = (this.delivery_data?.status == "In_transit")

      this.service.updateData(this.delivery_data);
    })

    if ('geolocation' in navigator) {
      this.getCurrentLocation();
    } else {
      console.error('Geolocation is not available in this browser.');
    }


  }

  //methode to get current location
  getCurrentLocation() {
    navigator.geolocation.getCurrentPosition(
      (position) => {

        this.currentlocation = {
          lat:  position.coords.latitude,
          lng:  position.coords.longitude,
        };

        //update current location with event
        if (this.delivery_id && this.currentlocation){
          setInterval(()=>  {
            this.service.sendEventLocationChanged({
              delivery_id : this.delivery_id,
              lng : this.currentlocation.lng,
              lat : this.currentlocation.lat
            })

            //change map delivery here
            if (this.delivery_data){
              this.delivery_data.location = this.currentlocation
              this.service.updateData(this.delivery_data);
            }
          },20000)
        }

      },
      (error) => {
        console.error('Error getting location:', error);
      }
    );
  }

  onPickUp() {
    this.service.sendEventStatusChanged({
      id : this.delivery_id,
      status : "picked-up"
    })
    this.toastr.success("Status changed to picked-up");
    this.isPickUp = false;
    this.isInTransit = true;
  }

  inTransit() {
    this.service.sendEventStatusChanged({
      id : this.delivery_id,
      status : "in-transit"
    })
    this.toastr.success("Status changed to in-transit");
    this.isInTransit = false;
    this.isDelivery = true;

  }

  onDelivery() {
    this.service.sendEventStatusChanged({
      id : this.delivery_id,
      status : "delivered"
    })
    this.toastr.success("Status changed to delivered");
    this.isDelivery = false;
    this.isFailed = false;
  }

  onFailed() {
    this.service.sendEventStatusChanged({
      id : this.delivery_id,
      status : "failed"
    })
    this.toastr.success("Status changed to failed");
    this.isDelivery = false;
    this.isFailed = false;
  }
}
