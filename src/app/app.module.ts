import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MapComponent } from './map/map.component';
import { DetailsComponent } from './details/details.component';
import { DriverComponent } from './driver/driver.component';
import { TrackerComponent } from './tracker/tracker.component';
import { AdminComponent } from './admin/admin.component';
import {CreatePackageComponent} from "./admin/create-package/create-package.component";
import {CreateDeliveryComponent} from "./admin/create-delivery/create-delivery.component";
import {FormsModule} from "@angular/forms";
import { ReactiveFormsModule } from '@angular/forms';
import { ToastrModule, ToastNoAnimationModule, ToastNoAnimation } from 'ngx-toastr';
import {AutocompleteLibModule} from 'angular-ng-autocomplete';

@NgModule({
  declarations: [
    AppComponent,
    MapComponent,
    DetailsComponent,
    DriverComponent,
    TrackerComponent,
    AdminComponent,
    CreatePackageComponent,
    CreateDeliveryComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    ToastrModule.forRoot(),
    ToastNoAnimationModule.forRoot(),
    AutocompleteLibModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
