import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {DriverComponent} from "./driver/driver.component";
import {TrackerComponent} from "./tracker/tracker.component";
import {AdminComponent} from "./admin/admin.component";
import {CreatePackageComponent} from "./admin/create-package/create-package.component";
import {CreateDeliveryComponent} from "./admin/create-delivery/create-delivery.component";

const routes: Routes = [
  { path: '', component: TrackerComponent, pathMatch: 'full' },
  { path : "web-driver", component : DriverComponent },
  { path : "web-tracker", component : TrackerComponent },
  { path : "web-tracker/:package_id", component : TrackerComponent },
  { path : "web-driver/:delivery_id", component : DriverComponent },
  { path : "web-admin", component : AdminComponent},
  { path : "create-package", component : CreatePackageComponent},
  { path : "create-delivery", component : CreateDeliveryComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
