import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {ServiceExternalService} from "./service-external.service";
import {NgForm} from "@angular/forms";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  title = 'gozem_angular_project';
  data : any
  package_id : any;
  inputSearch: any;
  textSearch : string = "Enter package ID";
  btnSearch : string = "Track";
  showSearchInput : boolean = true;


  constructor(private router: Router, private service : ServiceExternalService) {}


  seach(searchForm: NgForm) {
    this.package_id = searchForm.value.inputSearch;
    if (this.package_id){
        if (this.router.url.includes("web-admin") || this.router.url.includes("web-tracker")){
          this.webPackage();
          this.router.navigate(['/web-tracker',`${this.package_id}`]);
        }
        if (this.router.url.includes("web-driver")){
          this.webDriver();
          this.router.navigate(['/web-driver',`${this.package_id}`]); //delivery_id = package_id here
        }
    }
  }

  ngOnInit(): void {
    //init header view
    if (location.href.includes("web-tracker")){
      this.webPackage();
    }
    if (location.href.includes("web-driver")){
      this.webDriver();
    }
    if (location.href.includes("web-admin")){
      this.webAdmin();
    }
  }

  webDriver(){
    this.showSearchInput = true;
    this.textSearch = "Enter delivery ID";
    this.btnSearch = "Submit"
  }
  webPackage() {
    this.showSearchInput = true;
    this.textSearch = "Enter package ID";
    this.btnSearch = "Track"
  }

  webAdmin() {
    this.showSearchInput = false;
  }


}
