import {Component, OnInit} from '@angular/core';
import {NgForm} from "@angular/forms";
import {ServiceExternalService} from "../../service-external.service";
import {ToastrService} from "ngx-toastr";

@Component({
  selector: 'app-create-delivery',
  templateUrl: './create-delivery.component.html'
})
export class CreateDeliveryComponent implements OnInit{
  PACKAGES: any  = [];
  start_time: any;
  pickup_time: any;
  package_id: any;

  constructor(private appService : ServiceExternalService, private toastr: ToastrService) {
  }

  saveDelivery(form: NgForm) {

    if (!form.valid){
      this.toastr.error("You must required all field !");
    }else{
      let playload = {
        // start_time : this.start_time,
        // pickup_time : this.pickup_time,
        package_id : this.package_id
      }
       this.appService.saveDelivery(playload).subscribe((data:any)=>{
        if (data.status){
          this.toastr.success(data.message);
          form.reset();
        }else{
          this.toastr.error("Internal server error !");
        }
      })
    }
  }

  ngOnInit() {
    this.appService.getPackage().subscribe((packages : any)=>{
      packages.data.map((item:any)=>{
        if (!item.active_delivery_id)
          this.PACKAGES.push({id:item.package_id, name:'Package of '+item.from_name+' - '+item.to_name})
      })
    });
  }

}
