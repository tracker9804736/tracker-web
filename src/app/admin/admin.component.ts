import {
  ChangeDetectorRef,
  Component,
  OnInit,
  ViewChild,
  ViewContainerRef
} from '@angular/core';
import {ServiceExternalService} from "../service-external.service";

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {
  @ViewChild('create_package', {read: ViewContainerRef}) package_target: ViewContainerRef;

  constructor(
    private cdRef: ChangeDetectorRef,
    private appService : ServiceExternalService
  ) {}

  packages : any = []
  deliveries : any = []

  ngOnInit() {
      this.appService.getPackage().subscribe((packages : any)=>{
        this.packages = packages.data
      });

      this.appService.getDelivery().subscribe((deliveries : any)=>{
        this.deliveries = deliveries.data
      });


  }

}
