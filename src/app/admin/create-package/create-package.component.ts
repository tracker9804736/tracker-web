import {Component, NgZone, OnDestroy, OnInit} from '@angular/core';
import {NgForm} from "@angular/forms";
import {ServiceExternalService} from "../../service-external.service";
import {ToastrService} from "ngx-toastr";


@Component({
  selector: 'app-create-package',
  templateUrl: './create-package.component.html'
})
export class CreatePackageComponent implements OnInit {
   playload: any = {
    description : null,
    from_address : null,
    from_name : null,
    from_location : {lng : null, lat : null},
    to_name : null,
    to_address : null,
    to_location : {lng : null, lat : null},
    weight : null,
    height : null,
    depth : null
  }

  isLoadingResult: boolean;
  isLoadingResult2: boolean;
  keyword = 'name';
  from_data : any[];
  to_data : any[];
  FromLocation : any = null;
  from_text_location : any = null;
  to_text_location : any = null;
  ToLocation : any = null;



  constructor(private appService : ServiceExternalService,
              private toastr: ToastrService, public zone: NgZone) {}
  ngOnInit() {
    this.from_data = []
    this.to_data = []
  }

  async getServerResponse(val: string) {
    this.isLoadingResult = true;
    if (val && val !== '' ){
    await this.appService.searchPlace(val).subscribe((results : any)=>{
        if (results.features == undefined){
          this.from_data = [];
        }else{
          let features = results.features;
          features.map((item : any)=>{
            let properties = item.properties;
            let name = properties.address_line1 +', '+properties.country;
            let locationLng = properties.lon;
            let locationLat = properties.lat;
            this.from_data.push({id : properties.place_id, name : name, lng : locationLng, lat : locationLat})
          })
        }
        this.isLoadingResult = false;
      })
    }
  }

  async getServerResponseShipper(val2: string) {
    this.isLoadingResult2 = true;
    if (val2 && val2 !== '' ){
      await this.appService.searchPlace(val2).subscribe((results2 : any)=>{
        if (results2.features == undefined){
          this.to_data = [];
        }else{
          let features = results2.features;
          features.map((item : any)=>{
            let properties = item.properties;
            let name = properties.address_line1 +', '+properties.country;
            let locationLng = properties.lon;
            let locationLat = properties.lat;
            this.to_data.push({id : properties.place_id, name : name, lng : locationLng, lat : locationLat})
          })
        }
        this.isLoadingResult2 = false;
      })
    }
  }

  selectEvent(item : any) {
    if (item){
      this.FromLocation = item;
      this.from_text_location = `Longitude : ${item.lng} - Latitude : ${item.lat}`
    }
  }

  selectEvent2(item : any) {
    if (item){
      this.ToLocation = item;
      this.to_text_location = `Longitude : ${item.lng} - Latitude : ${item.lat}`
    }
  }

  searchCleared() {
    this.from_data = [];
    this.to_data = [];
    this.FromLocation  = null;
    this.ToLocation  = null;
    this.from_text_location = null;
    this.to_text_location = null;
  }

  async savePackage(form : NgForm){
     if (!form.valid || !this.FromLocation || !this.ToLocation){
        this.toastr.error("You must required all field !");
     }else{
       this.playload = form.value;
       if (this.FromLocation){
         this.playload.from_location = {lng : this.FromLocation.lng, lat : this.FromLocation.lat};
         this.playload.from_address = this.FromLocation.name;
       }
       if (this.ToLocation){
         this.playload.to_location = {lng : this.ToLocation.lng, lat : this.ToLocation.lat};
         this.playload.to_address = this.ToLocation.name;
       }
       await this.appService.savePackage(this.playload).subscribe((data:any)=>{
         if (data.status){
           this.toastr.success(data.message);
           form.reset();
           this.searchCleared();
         }else{
           this.toastr.error("Internal server error !");
         }
       })
     }

  }



}

