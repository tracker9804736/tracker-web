import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import { Environment } from "../environments/environment";
import {BehaviorSubject} from "rxjs";
import { io } from "socket.io-client";

@Injectable({
  providedIn: 'root'
})
export class ServiceExternalService {


  private dataSubject = new BehaviorSubject<any>({});
  public data$ = this.dataSubject.asObservable();

  private eventSubject = new BehaviorSubject<any>(null);
  public event$ = this.eventSubject.asObservable();


  updateData(data: any) {
    this.dataSubject.next(data);
  }

  socket = io('ws://localhost:3002');


  public sendEventLocationChanged(data: any) {
    console.log('location_changed');
    this.socket.emit('location_changed', data);
  }

  public sendEventStatusChanged(message: any) {
    this.socket.emit('status_changed', message);
  }

  public getNewEventDeliveryUpdated = (id : string) => {
    this.socket.on('delivery_updated'+id, (message : any) =>{
      if (message)
      this.eventSubject.next(message);
    });
  };

  rootUrl = '/api/'
  constructor(private http : HttpClient) { }

  savePackage(data : any){
    return this.http.post(Environment.API_URL+this.rootUrl+'package', data)
  }

  saveDelivery(data : any){
    return this.http.post(Environment.API_URL+this.rootUrl+'delivery', data)
  }
  getPackage(){
    return this.http.get(Environment.API_URL+this.rootUrl+'package')
  }

  getDelivery(){
    return this.http.get(Environment.API_URL+this.rootUrl+'delivery')
  }

  searchPlace(text:string){
    return this.http.get(`https://api.geoapify.com/v1/geocode/autocomplete?text=${text}&apiKey=006b4272b5b44b69ac092efba6749afa`)
  }

  findPackage(id: string) {
    return this.http.get(Environment.API_URL+this.rootUrl+'package/'+id)
  }

  findDelivery(id: string) {
    return this.http.get(Environment.API_URL+this.rootUrl+'delivery/'+id)
  }


}
